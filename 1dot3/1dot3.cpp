// 1dot3.cpp : Defines the entry point for the console application.
//

/*
	1.3 The for statement
*/
#include "stdafx.h"


int main()
{
	int fahr; // Variable that will be used as the counter and for the condition checker of the following for loop

	for (fahr = 0; fahr <= 300; fahr = fahr + 20) /* Initialize fahr to 0, while it's lower or equal to 300, increment by 20 */{
		printf("%3d %6.1f\n", fahr, (5.0/9.0) * (fahr-32)); // Print the value of fahr (which is equal to celsius), calculate and print the result for fahrenheit
	}

	printf("\n\nPrinting in reverse order\n\n");

	for (fahr = 300; fahr >= 0; fahr = fahr - 20) /* Reverse the printing order of the previous for loop */ {
		printf("%3d %6.1f\n", fahr, (5.0 / 9.0) * (fahr - 32)); // Print the value of fahr (which is equal to celsius), calculate and print the result for fahrenheit
	}
	return 0;
}

