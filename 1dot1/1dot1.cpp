// 1dot1.cpp : Defines the entry point for the console application.
//
/*
1.1 Getting Started

Print "sup world" on the screen.

*/

#include "stdafx.h"

int main()
{
	printf("sup \a A world\n"); // Prints "sup world" on the console
	return 0; // Ends the program successfully
}

