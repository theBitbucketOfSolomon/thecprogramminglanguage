// 1dot4.cpp : Defines the entry point for the console application.
//

/*
1.4 Symbolic Constants
*/

#include "stdafx.h"

#define LOWER 0 /* Lower limit of table, LOWER will be replaced by 0 when compiling */
#define UPPER 300 /* Upper limit of table */
#define STEP 20 /* Step size */

int main()
{
	/* Print Fahrenheit to Celsius table */
	int fahr;
	for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP) {
		printf("%3d %6.1f\n", fahr, (5.0 / 9.0)*(fahr - 32));
	}
	return 0;
}

