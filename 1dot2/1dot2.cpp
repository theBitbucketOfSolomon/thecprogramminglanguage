// 1dot2.cpp : Defines the entry point for the console application.
//
/*
1.2 Variables and Arithmetic Expressions
Print the fahrenheit-celsius table
From 0 fahr to 300 fahr
*/
#include "stdafx.h"


int main()
{
	// Integer based Fahrenheit to Celsius calculator
	//int celsius; // Celsius
	//int lower = 0; // Lower limit of temperature scale
	//int upper = 300; // Upper limit of temperature scale
	//int step = 20; // Step size
	//int fahr = lower;  // Fahrenheit, create and set to lower limit
	//while (fahr <= upper){ /* While fahrenheit is lower or equal to upper limit*/
	//	celsius = 5 * (fahr - 32) / 9; // Calculate celsius
	//	/* printf("%d\t%d\n", fahr, celsius); // Print Fahrenheit value, Celsius value */
	//	printf("%3d %6d\n", fahr, celsius); // Print Fahrenheit and Celsius value with better formatting (Right, justified)
	//	fahr = fahr + step; // Increase Fahrenheit by step size value
	//}
	// Initialization
	float celsius; // Celsius, no value assigned
	float lower = 0; // Lower limit of temperature scale
	float upper = 300; // Upper limit of temperature scale
	float step = 20; // Step size
	
	// Program
	float fahr = lower; // Fahrenheit, create and set to lower limit
	printf("FAHRENHEIT\tCELSIUS\n"); // Table headers
	while (fahr <= upper) /* While fahrenheit is lower or not equal to upper limit*/ {
		celsius = (5.0 / 9.0) * (fahr - 32.0); // Calculate celsius, now with better accuracy
		printf("%7.0f %15.2f\n", fahr, celsius); // Print Fahrenheit and Celsius value with better formatting (Right, justified)
		fahr = fahr + step; // Increase Fahrenheit by step size value
	}
	printf("\n\nGenerating Celsius to Fahrenheit table...\n\n");
	/* The Celsius table will print a table of converted celsius values to fahrenheit, from 0C to 100C, in 5C increments */
	step = 5; // Change step size
	upper = 100; // Change upper limit
	celsius = lower; // Set celsius to lower limit

	printf("CELSIUS\tFAHRENHEIT\n"); // Table headers
	while (celsius <= upper) /* While celsius is lower or not equal to upper limit */{
		fahr = celsius * (9.0 / 5.0) + 32; // Calculate fahrenheit value, assign to variable
		printf("%5.0f %10.0f\n", celsius, fahr); // Print Celsius and Fahrenheit value
		celsius = celsius + step; // Increase Celsius by step size value
	}
	return 0;
}

