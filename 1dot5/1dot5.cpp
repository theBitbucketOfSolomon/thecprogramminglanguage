// 1dot5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*
1.5 Character Input and Output
*/

int main()
{
	int c; // will hold characters read
	printf("Value of function getchar() != EOF: %d", c = (getchar() != EOF)); /* verifies if the character read is the EOF */
	printf("\nThe value of EOF is: %d\n", EOF); // Prints the value assigned to the defined value EOF

	/* Count characters in input */
	double nc; // variable that will hold the character count
	for (nc = 0; (c = getchar()) != 'q'; ++nc) /* initialize the counter on 0, while we're not done reading characters, increase counter by one */
		; /* break the for loop with q */
	printf("\nAmount of characters in input: %.0f\n", nc); /* print amount of characters read */

	/* Count new lines, tabs and blanks in input */
	int nl = 0; // will be the new line counter
	int blanksCounter = 0; /* variable that will hold the amount of blanks (spaces) read by the input, initialized at 0 */
	int tabsCounter = 0; /* variable that will hold the amount of tabs read by the input, initialized at 0 */
	printf("\nCounting new lines, blanks, and tabs:\n");
	while ((c = getchar()) != 'q') /* While we're not done reading characters */ {
		if (c == '\n') /* If the integer value of the character read equals to the integer value of new line */{
			++nl; // increment the new line counter
		}

		if (c == ' ') /* If the integer value of the character read equals the integer value of the blank/space character */ {
			++blanksCounter; /* Increment blanks counter */
		}

		if (c == '\t') /* If the integer value of the character read equals the integer value of a tab character */ {
			++tabsCounter; /* Increment tabs counter */
		}

		/* Break while loop with q */
	}
	printf("Total amount of new lines: %d\n", nl); /* print amount of new lines counted at the time */
	printf("Total amount of blank/spaces: %d\n", blanksCounter); /* print amount of blank/spaces counted at the time */
	printf("Total amount of tabs: %d\n", tabsCounter); /* print amount of tabs counted at the time */

	printf("\nCopy input to its output, replace each string of one or more blanks by a single blank\n");


	c = 0; // Reset character integer value
	while ((c = getchar()) != EOF) /* Break loop with EOF */ {
		/* If it's a blank only print once */
		putchar(c);
		while ((c = getchar()) == ' ') /* While we're reading blanks, do nothing */{
			;
		}
	}

	printf("\nCopy its input to output, replace each tab with \\t each backspace with \\b and each backslash with \\\n");

	c = 0; // Reset character integer value
	while ((c = getchar()) != EOF) /* While we're not done reading characters */ {
		
		if (c == '\t') /* If the integer value of the character is a tab */{
			printf("\\t"); /* Display \t instead of a tab */
		}
		else {

			if (c == '\b') /* If the integer value of the character is a backspace */{
				printf("\\b"); /* Display \b instead of a backspace */
			}
			else {

				if (c == '\\') /* If the integer value of the character is a backslash */{
					printf("\\\\"); /* Display \\ instead of \ */
				}
				else /* The integer value of the character isn't a tab, backspace, or backslash */ {
					putchar(c); /* Display on screen */
				}
			}
		}
	}

	
	return 0;
}

